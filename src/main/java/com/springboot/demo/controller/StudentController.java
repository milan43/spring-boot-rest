package com.springboot.demo.controller;

import com.springboot.demo.model.Student;
import com.springboot.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*

 @Author melone
 @Date 7/14/18 
 
 */
@RestController
@RequestMapping("/students")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @RequestMapping("")
    public List<Student> getAllStudents() {
        return studentService.getAll();
    }

    @RequestMapping("/{id}")
    public Student getById(@PathVariable("id") int id) {
        return studentService.getById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") int id) {
        studentService.delete(id);
    }

    @RequestMapping( method = RequestMethod.POST)
    public void add(@RequestBody Student student) {
        studentService.add(student);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable int id, @RequestBody Student student) {
        studentService.update(id, student);
    }
}