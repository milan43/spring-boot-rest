package com.springboot.demo.service;

import com.springboot.demo.model.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*

 @Author melone
 @Date 7/14/18 
 
 */

@Service
public class StudentService {
    List<Student> students = new ArrayList<>(Arrays.asList(new Student(1, "Jack", 15),
            new Student(2, "Hari", 12),
            new Student(3, "Ahy", 17)));

    public List<Student> getAll() {
        return students;
    }

    public Student getById(int id) {
        return students.stream().filter(s -> s.getId() == id).findFirst().get();
    }

    public void delete(int id){
        //students.remove(getById(id));
        students.removeIf(s->s.getId()==id);
    }
    public void add(Student student){
        students.add(student);
    }
    public void update(int id, Student student){
      students.forEach(s->{
          if(s.getId()==id){
              students.set(id-1,student);
              return;
          }
      });
    }
}







